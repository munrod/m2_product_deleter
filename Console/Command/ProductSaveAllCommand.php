<?php
namespace Utility\ProductDeleter\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Magento\Catalog\Model\ProductFactory as Product;
use Magento\Framework\App\State;
use Magento\Framework\Registry;
use Magento\Framework\App\Area;

class ProductSaveAllCommand extends Command
{
    private $product;
    private $productIds = array();
    private $registry;

    public function __construct(
        Product $product,
        State $state,
        Registry $registry
    )
    {
        $this->registry = $registry;
        $this->product = $product->create();
        $this->productIds = array('8468');

        try{
            $state->setAreaCode(Area::AREA_ADMINHTML);
        }catch(\Magento\Framework\Exception\LocalizedException $e){

        }

        parent::__construct('utility:productsave:all');
    }

    protected function configure()
    {
        $this->setName('utility:productsave:all')->setDescription('Saves all products');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->registry->register('isSecureArea', true, true);
        $this->saveAllProducts();


    }

    private function getProductCollection()
    {
        return $this->product->getCollection();

    }

    private function saveAllProducts()
    {
        foreach($this->getProductCollection() as $product){
            echo 'Saving: ' . $product->getId() . PHP_EOL;
            $product->save();
        }
    }
}
