<?php
namespace Utility\ProductDeleter\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Magento\Catalog\Model\ProductFactory as Product;
use Magento\Framework\App\State;
use Magento\Framework\Registry;
use Magento\Framework\App\Area;
use Exception;

class ProductDeleteRangeCommand extends Command
{
    private $product;
    private $productIds = array();
    private $registry;

    public function __construct(
        Product $product,
        State $state,
        Registry $registry
    )
    {
        $this->registry = $registry;
        $this->product = $product->create();
        $this->productIds = array('8468');

        try{
            $state->setAreaCode(Area::AREA_ADMINHTML);
        }catch(\Magento\Framework\Exception\LocalizedException $e){

        }

        parent::__construct('utility:productdelete:range');
    }

    protected function configure()
    {
        $this->setName('utility:productdelete:range')
            ->setDescription('Deletes a range of products using a json file, the json file is product_delete_range.json in the var dir');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->registry->register('isSecureArea', true, true);
        $this->deleteProductRange();
    }

    private function getProductCollection()
    {
        return $this->product->getCollection();

    }

    private function getIdsFromFile()
    {
        $filePath = '/var/www/vhosts/partyshowroom/production/htdocs/var/product_delete_range.json';
        $jsonData = file_get_contents($filePath);
        $entityIds = array();
        foreach (json_decode($jsonData, true) as $entity) {
            $entityIds[] = $entity['entity_id'];
        }
        return $entityIds;
    }

    private function deleteProductRange()
    {
        foreach ($this->filterProductIds() as $product) {
            try {
                $this->product->getResource()->delete($product);
                echo 'Deleted: ' . $product->getId() . PHP_EOL;
            } catch (Exception $e) {
                echo $e->getMessage();
            }
        }
    }

    private function filterProductIds()
    {
        return $this->getProductCollection()->addAttributeToFilter('entity_id', array('in' => $this->getIdsFromFile()));
    }

}
